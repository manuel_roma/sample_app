module SessionsHelper

  # loguea el usuario actual
  def log_in(user)
    session[:user_id] = user.id
  end


  # desloguea el usuario actual
  def log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end


  # recuerda un usuario en una sesion persistente
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end


  # comprueba si el usuario dado es el usuario actual
  def current_user?(user)
    user == current_user
  end


  # olvida una sesion persistente
  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end


  # devuelve el usuario de la cookie remember token
  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end


  # comprueba si el usuario esta logueado  
  def logged_in?
    !current_user.nil?
  end
  

  # redirige a pagina guardada (o defecto)
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end


  # guarda la URL a la que se intenta acceder
  def store_location
    session[:forwarding_url] = request.url if request.get?
  end
  
end
